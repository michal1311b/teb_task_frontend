import axios from 'axios';

export class APIService {
    getStates() {
        const api = this.getEnvUrl();
        const url = `${api}/api/get-states`;
        let data = axios.get(url);
        return data;
    };
    storeKeys(data) {
        const api = this.getEnvUrl();
        const url = `${api}/api/store-keys`;
        return axios.post(url, {
            body: data
        });
    };
    getEnvUrl() {
        if(process.env.NODE_ENV === 'development') {
            var API_URL = 'http://127.0.0.1:8008';
            return API_URL;
        } else {
            var API_URL = 'http:/somedomain.com';
            return API_URL;
        }
    }
}