export class TextService {
  removeAccents(strAccents) {
    var strAccents = strAccents.split('');
    var strAccentsOut = new Array();
    var strAccentsLen = strAccents.length;
    var accents = 'ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž';
    var accentsOut = "AAAAAAaaaaaaOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz";
    for (var y = 0; y < strAccentsLen; y++) {
      if (accents.indexOf(strAccents[y]) != -1) {
        strAccentsOut[y] = accentsOut.substr(accents.indexOf(strAccents[y]), 1);
      } else {
        strAccentsOut[y] = strAccents[y];
      }
    }
    strAccentsOut = strAccentsOut.join('');
    
    return strAccentsOut;
  };
  generateText(userLetters, uuid) {
    let result = [];

    let text1 = this.removeAccents(userLetters).split('');
    let text2 = this.removeAccents(uuid).split('');
    let i, l = Math.min(text1.length, text2.length);

    for (i = 0; i < l; i++) {
      result.push(text1[i], text2[i]);
    }
    result.push(...text1.slice(l), ...text2.slice(l));

    return result.join('');
  };
  randomInt(min, max) {
    return min + Math.floor((max - min) * Math.random());
  };
}