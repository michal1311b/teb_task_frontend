import Vue from 'vue'
import Router from 'vue-router'
import FirstStep from '@/components/FirstStep'
import SecondStep from '@/components/SecondStep'
import ThirdStep from '@/components/ThirdStep'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'FirstStep',
      component: FirstStep,
      props: true
    },
    {
      path: '/2',
      name: 'SecondStep',
      component: SecondStep,
      props: true
    },
    {
      path: '/3',
      name: 'ThirdStep',
      component: ThirdStep,
      props: true
    }
  ]
})
