import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',

  state: {
    email: '',
    state_id: '',
    uuid: '',
    dates: []
  },

  mutations: {
    setEmail(state, payload) {
      state.email = payload.email;
    },
    setDates(state, payload) {
      state.dates = payload.dates;
    },
    setStateId(state, payload) {
      state.state_id = payload.state_id;
    },
    setStateDate(state, payload) {
      state.dates = payload.dates;
    }
  }
});